#SETUP
#THANKS TO https://simpleit.rocks/python/how-to-translate-a-python-project-with-gettext-the-easy-way/ and https://inventwithpython.com/blog/2014/12/20/translate-your-python-3-program-with-the-gettext-module/ for their gettext guides!

# THANKS TO @ErdoganOnal for their comment on this SO question: https://stackoverflow.com/questions/61621821/any-secure-alternatives-for-this?noredirect=1#comment109002742_61621821 That comment helped with the Press Any Key To Continue function

# THANKS TO https://stackoverflow.com/questions/33594958/is-it-possible-to-align-a-print-statement-to-the-center-in-python FOR showing how to ALIGN the PRINT STATEMENT
#
from cprint import cprint
import sys, os, logging #sys so I can exit, os so I can do I can't remember, logging so I can log.
logging.basicConfig(filename="palc.log", level=logging.DEBUG) #set up logging
try:
    import msvcrt
    _IS_WINDOWS = True
    logger.info("Imported msvcrt")
except ImportError:
    import tty
    import termios
    _IS_WINDOWS = False
    logging.info("Imported tty, termios")
import gettext #to translate Palc
language = input("English or Francais? (do not add accents to letters/n'ajoute pas les accents aux lettres): ")
language = language.lower()
if language == "francais":
    logging.info("Set language to French")
    gettext.bindtextdomain('base', localedir="locales")
    lang_translations = gettext.translation('base',localedir='locales', languages=["fr"])
elif language == "english":
    logging.info("Set language to English")
    gettext.bindtextdomain('base', localedir="locales")
    l_translations = gettext.translation('base', localedir='locales', languages=["en"])
try:
    lang_translations.install()
    _ = lang_translations.gettext
except:
    l_translations.install()
    _ = l_translations.gettext
from sys import exit as e #so that we can exit later on
import time
try:
    from func import *
except ImportError:
    logging.critical("Could not access file func.py")
    cprint.fatal(_("I can't access the file func.py. This file is necessary for proper function of the Software."), interrupt=True)
cprint.ok(_("Loading...............\n"))
time.sleep(2)
def palc():
    while True:
       print(_("Press any key to continue..."), end="", flush=True)
       if _IS_WINDOWS:
           msvcrt.getch()
       else:
           fd = sys.stdin.fileno()
           settings = termios.tcgetattr(fd)
           try:
               tty.setraw(sys.stdin.fileno())
               sys.stdin.read(1)
           finally:
               termios.tcsetattr(fd, termios.TCSADRAIN, settings)
       print(chr(27)+'[2j') #First attempt at clearing the screen with ANSI escape codes.
       print('\033c') #Second attempt at clearing the screen with ANSI escape codes.
       print('\x1bc') #Third attempt at clearing the screen with ANSI escape codes.
#CALCULATION CHOICE
       calc = input(_("What calculation do you wish to do? (Type `?' for a list of commands)\nType: "))
       calc = calc.lower() #make variable "calc" lowercase
#HELP
       if "?" in calc:
           logging.info("User needed help")
           h()
       elif "help" in calc:
           logging.info("User needed help")
           h()
#TAX
       elif "tax" in calc:
            taxCalc()
#MULTIPLICATION
       elif "*" in calc:
            multi()
       elif "x" in calc:
            multi()
#SQUARE
       elif "sq" in calc:
            n = int(input(_("Number? ")))
            cprint.info(n * n)
            logging.info("User squared number %s got result %s" % (n, (n * n)))
       elif "[]" in calc:
            n = int(input(_("Number? ")))
            logging.info("User squared number %s got result %s" % (n, (n * n)))
            cprint.info(n * n)
#DIVISION
       elif "/" in calc:
            div()
       elif "div" in calc:
            div()
#SUBTRACTION
       elif "-" in calc:
            sub()
       elif "sub" in calc:
            sub()
       elif "min" in calc:
            sub()
#ADDITION
       elif "+" in calc:
            add()
       elif "add" in calc:
            add()
#MODULO
       elif "%" in calc:
            mod()
       elif "mod" in calc:
            mod()
#AREA
       elif "ar" in calc:
            area()
       elif "#" in calc:
            area()
       elif "area" in calc:
            area()
#VOLUME
       elif "vol" in calc:
            uc()
#CUBE
       elif "{}" in calc:
            cubedNumber = int(input(_("\nType the number to be cubed: ")))
            print()
            cprint.info(cubedNumber ** 3) #Manually cube number
            logging.info(("User cubed number ", cubedNumber, " got result ", (cubedNumber ** 3)))
            print()
#EXIT
       elif "quit" in calc:
            logging.info("User exited using `quit' command")
            e()
#EXPONENTS
       elif "power" in calc:
            origin = float(input(_("Original number?")))
            ex = float(input(_("Exponent? ")))
            cprint.info(origin ** ex)
            logging.info(("User exponented number ", origin, " with ", ex, "getting ", (origin ** ex)))
       elif "ex" in calc:
            origin = float(input(_("Original number?")))
            ex = float(input(_("Exponent? ")))
            print(origin ** ex)
            logging.info(("User exponented number ", origin, " with ", ex, "getting ", (origin ** ex)))
#CUBE TWICE
       elif "{2}" in calc:
            cprint.err(_("That feature was discontinued."))
            logging.err("User attempted to use cube twice function")
#ROOTS
       elif "root" in calc:
            root = input(_("Square root or cube root?(square/cube)"))
            root = root.lower()
            if "square" in root:
                num = input(_("Number to be rooted?"))
                cprint.info(_("That equals.....\n", num ** 0.5))
                logging.info(("user sqrooted number ", (num**0.5)))
            elif "cube" in root:
                cu()
            else:
                print(_("Currently I don't support the root you chose. Hopefully this will change :)"))
#EASTER EGG!
       elif "=" in calc:
            print()
            number = int(input(_("Type in a number: ")))
            if number == 42:
                cprint.info(_("=42 -- the answer to life, the universe, and everything"))
                logging.info("User got the easter egg")
            else:
                cprint.err(_("Do you really need a calculator for this?"))
                logging.info(("User used the `=' feature for number ", number))
#NUMBER SYSTEMS
       elif "base" in calc:
            base()
#ORD
       elif "ord" in calc:
           logging.info(("User ord'ed to get result ", result))
           result = str(ord(int(input(_("Type in the number to ord: ")))))
           cprint.info("=", result)
#LOGARITHM
       elif "log" in calc:
           log()
#MEMORY
       elif "mem" in calc:
            memOrRecall = input(_("Would you like to set the memory or recall? (set / recall)\nType: "))
            if memOrRecall.lower() in "set":
                remember()
            elif memOrRecall.lower() in "recall":
                readMyMemory()
            else:
                cprint.err(_("You did not type an answer.\nAbort."))
                logging.error("User didn't type an answer in MEM function")
#FIBONACCI
       elif "fib" in calc:
            cprint.ok(_("Starting fibonacci sequence. Please wait."))
            fib()
#PERCENTAGE
       elif "percent" in calc: #SOURCE: https://stackoverflow.com/a/5998010/9654083
            whichOne = int(input(_('''1 - Calculate "What is x% of y?"
2 - Convert a number to percentage.
Type: ''')))
            if whichOne == 1:
                whatIsPercent()
            elif whichOne == 2:
                getPercentageRN()
            else:
                cprint.err(_("You didn't type a valid answer. Abort."))
#INTEREST
       elif "interest" in calc:
            calculateInterest()
#TEMPERATURE
       elif "temperature" in calc:
            tempCalc()
#OTHERWISE
       elif calc == "":
            logging.error("User attempted to type nothing as a command")
            print(_("Type something!"))
       elif calc is None:
            logging.error("User attempted to type nothing as a command")
            print(_("Type something!"))
       else:
            logging.error("User typed an invalid command")
            print(_('''
            I don't understand your request. Here are the currently supported calculations:
            * or x; / or div; -, min, or sub; + or add; % or mod (modulo); sq or [] (square); ar or # (area); vol (volume); {} (cube); power (exponents/power); root (roots); = (equals); fib (fibonacci) log (logarithm); mem (memory); percent (calculate percentage); interest (interest calculator); temperature (convert Celsius to Farenheit etc); and base (convert number system). Sorry for the inconvenience
            '''))
width = os.get_terminal_size().columns
for i in range(0, width):
    print("-", sep="", end="")
logging.info(("Printed ", width, "dashes"))
cprint.info(_("Welcome to Palc!".center(width)))
for i in range(0, width):
    print("-", sep="", end="")
logging.info(("Printed ", width, "dashes"))
try:
    palc() #run all that code
except KeyboardInterrupt: #if ^C
    logging.info("KeyboardInterrupt")
    cprint.ok(_("\nNote that you CAN type `quit' instead of pressing the interrupt key"))
    sys.exit()
except EOFError: #if ^D
    logging.info("EOFError")
    cprint.ok(_("\nWhy ^D? Why not just type `quit'?"))
    sys.exit()
except (ValueError, TypeError):
    logging.critical("ValueError or TypeError")
    width = os.get_terminal_size().columns
    for i in range(0, width):
        print("-", sep="", end="", flush=True)
    logging.info(("Printed ", width, "dashes"))
    cprint.err(_("\aERROR!".center(width)))
    for i in range(0, width):
        print("-", sep="", end="", flush=True)
    logging.info(("Printed ", width, "dashes"))
    cprint.fatal(_("You typed in an invalid integer or float. Or maybe the program needs debugging. Either way, it's a pretty big error."))
    cprint.ok(_("There may have been details before the word `ERROR!'. Check that."))
    e()
except SystemExit:
    cprint.ok(_("Looks like you exited."))
except:
    width = os.get_terminal_size().columns
    for i in range(0, width):
        print("-", sep="", end="", flush=True)
    logging.info(("Printed ", width, "dashes"))
    cprint.fatal(_("Unknown Error!".center(width)))
    for i in range(0, width):
        print("-", sep="", end="", flush=True)
    logging.info(("Printed ", width, "dashes"))
    cprint.fatal(_("An unknown error occured. Please file an Issue at github.com/thetechrobo/support."))
finally:
    logging.info("Program stopped execution.")
#EOF